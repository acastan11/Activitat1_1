package llibreria;

public class NewMain {

    public static void main(String[] args) {

        int edat = llibreria.MyUtils.edat(3, 11, 1867);
        System.out.println(edat);

        String inv = llibreria.MyUtils.inverteix("Bon dia");
        System.out.println(inv);

        double fac = llibreria.MyUtils.factorial(6);
        System.out.println(fac);

        {

        }
    }

}
