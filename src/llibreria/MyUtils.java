/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package llibreria;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author Alex Castan
 */
public class MyUtils {

    public static String inverteix(String cadena) {
        if (cadena == "") {
            return "-1";
        }
        String resultat = "";
        for (int i = cadena.length() - 1; i >= 0; i--) {
            resultat += cadena.charAt(i);
        }
        return resultat;
    }

    /**
     *
     * @param day int dia del naixement
     * @param month int mes del naixement
     * @param year int any del naixement
     * @return edat de la persona, per edat>150 retorna -1, per dates
     * impossibles retorna -2
     *
     */
    public static int edat(int day, int month, int year) {
        int resultat = 0;

        Calendar calendarBirth = new GregorianCalendar();
        calendarBirth.setLenient(false);
        calendarBirth.set(year, month, day);

        Calendar calendar = Calendar.getInstance();

        int any = calendar.get(Calendar.YEAR);

        int mes = calendar.get(Calendar.MONTH);

        mes = mes + 1;

        int dia = calendar.get(Calendar.DAY_OF_MONTH);

        if (year > any) {
            return -2;
        }
        if(month >12||month<1||day>31||day <1){
            return -2;
        }
        if (year == any && month > mes) {
            return -2;
        }
        if (year == any && month == mes && day > dia) {
            return -2;
        }

        //
        if (month % 2 == 0 && day > 31) {
            return -2;
        } else if (month % 2 != 0 && day > 30) {
            return -2;
        } else if (month == 2 && day > 28) {
            return -2;
        }
        //

        if ((day > dia && month == mes) || month > mes) {
            resultat = any - year - 1;
            if (resultat > 150) {
                return -1;
            }
            return resultat;

        } else {
            resultat = any - year;
            if (resultat > 150) {
                return -1;
            }
        }
        return resultat;
    }

    /**
     *
     * @param numero nÃºmero del que es calcula el factorial
     * @return retorna el factorial d'un nÃºmero. Si el nombre es negatiu
     * retorna -1.
     */
    public static double factorial(double numero) {

        if (numero < 0) {
            return -1;
        }
        if (numero == 0) {
            return 1;
        } else {
            double resultat = numero * factorial(numero - 1);
            return resultat;
        }
    }
}
