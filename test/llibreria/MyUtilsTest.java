/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package llibreria;

import java.util.Calendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Alex Castan
 */
public class MyUtilsTest {
    
    public MyUtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of inverteix method, of class MyUtils.
     */
    @Test
    
    public void testInverteix() {
        System.out.println("inverteix");
        assertEquals ("aloh", MyUtils.inverteix("hola"));//correcte per provar
        assertEquals ("-1", MyUtils.inverteix(""));
        assertEquals ("aid noB", MyUtils.inverteix("Bon dia"));
        
        // TODO review the generated test code and remove the default call to fail.
       
    }

   
    /**
     * Test of factorial method, of class MyUtils.
     */
    @Test
    public void testFactorial() {
        System.out.println("factorial");
        assertEquals(1, MyUtils.factorial(0), 0);
        assertEquals(24, MyUtils.factorial(4), 0);
        assertEquals(-1, MyUtils.factorial(-1), -1);
        

        
        // TODO review the generated test code and remove the default call to fail.
      
    }
    


    @Test
    public void testEdat() {
        System.out.println("edat");
        Calendar calendar = Calendar.getInstance();

        int any = calendar.get(Calendar.YEAR);

        int mes = calendar.get(Calendar.MONTH);
        mes = mes + 1;

        int dia = calendar.get(Calendar.DAY_OF_MONTH);
        
        
        
        assertEquals (-2, MyUtils.edat(31, 11, 2017));
        assertEquals (20, MyUtils.edat(11, 1, 1997));
        assertEquals (-1, MyUtils.edat(11, 11, 1650));
        assertEquals (0, MyUtils.edat(4, 11, 2017));
        assertEquals (-2, MyUtils.edat(12, 11, 2017));
        assertEquals (150, MyUtils.edat(3, 11, 1867));
        assertEquals (-2, MyUtils.edat(3, 14, 1867));
        assertEquals (-2, MyUtils.edat(34, 11, 1867));
                
       
}
    
}

